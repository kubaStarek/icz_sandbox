package com.jst.jaxb;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Date;

public class JaxbTest {

    public static void main(String[] args) throws Throwable {
        MDConsValues consValues = new MDConsValues();
        consValues.setDateDisposal(new Date());
        consValues.setDateClosed(new Date());
        consValues.setRdCntrl(new MDConsValues.RdCntrl());
            consValues.getRdCntrl().setAcr("A10");
            consValues.getRdCntrl().setAction("A");
            consValues.getRdCntrl().setPeriod(10);
            consValues.getRdCntrl().setEventType("AFTER_ENTITY_CLOSED");
            consValues.getRdCntrl().setTitle("Dokumenty určené k archivaci");
            consValues.getRdCntrl().setReason("Instrukce MVČR ze dne 25. 5. 1992 čj. VSC/1-793/92");

        String xml = toXml(consValues, MDConsValues.class);
        System.out.println(xml);
        MDConsValues cv2 = fromXml(xml, MDConsValues.class);
        System.out.println();

        String a = null;
        "B".equals(a);
    }

    public static <T> String toXml(T jaxbObject, Class<T> jaxbClass) throws JAXBException{
        return toXml(jaxbObject, jaxbClass, false);
    }

    public static <T> String toXml(T jaxbObject, Class<T> jaxbClass, boolean formattedOutput) throws JAXBException{
        JAXBContext context = JAXBContext.newInstance(jaxbClass);
        Marshaller mar= context.createMarshaller();
        mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, formattedOutput);
        mar.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

        Writer writer = new StringWriter();
        mar.marshal(jaxbObject, writer);
        return writer.toString();
    }

    public static <T> T fromXml(String xml, Class<T> jaxbClass) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(jaxbClass);
        return (T) context.createUnmarshaller()
                .unmarshal(new StringReader(xml));
    }
}
