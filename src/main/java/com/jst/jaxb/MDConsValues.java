package com.jst.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "dateDisposal",
        "dateClosed",
        "rdCntrl"
})
@XmlRootElement(name = "ConsValues")
public class MDConsValues {

    @XmlElement(name = "DateDisposal")
    private Date dateDisposal;

    @XmlElement(name = "DateClosed")
    private Date dateClosed;

    @XmlElement(name = "RdCntrl")
    private RdCntrl rdCntrl;

    public Date getDateDisposal() {
        return dateDisposal;
    }

    public void setDateDisposal(Date dateDisposal) {
        this.dateDisposal = dateDisposal;
    }

    public Date getDateClosed() {
        return dateClosed;
    }

    public void setDateClosed(Date dateClosed) {
        this.dateClosed = dateClosed;
    }

    public RdCntrl getRdCntrl() {
        return rdCntrl;
    }

    public void setRdCntrl(RdCntrl rdCntrl) {
        this.rdCntrl = rdCntrl;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "RdCntrl", propOrder = {
            "acr","action", "period", "eventType", "title", "reason"})
    public static class RdCntrl{

        @XmlElement(name = "Acr")
        private String acr;

        @XmlElement(name = "Aaction")
        private String action;

        @XmlElement(name = "Period")
        private int period;

        @XmlElement(name = "EventType")
        private String eventType;

        @XmlElement(name = "Title")
        private String title;

        @XmlElement(name = "Reason")
        private String reason;

        public String getAcr() {
            return acr;
        }

        public void setAcr(String acr) {
            this.acr = acr;
        }

        public String getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }

        public int getPeriod() {
            return period;
        }

        public void setPeriod(int period) {
            this.period = period;
        }

        public String getEventType() {
            return eventType;
        }

        public void setEventType(String eventType) {
            this.eventType = eventType;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        @Override
        public String toString() {
            return "RdCntrl{" +
                    "acr='" + acr + '\'' +
                    ", action='" + action + '\'' +
                    ", period=" + period +
                    ", eventType='" + eventType + '\'' +
                    ", title='" + title + '\'' +
                    ", reason='" + reason + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "MDConsValues{" +
                "dateDisposal=" + dateDisposal +
                ", dateClosed=" + dateClosed +
                ", rdCntrl=" + rdCntrl +
                '}';
    }
}
