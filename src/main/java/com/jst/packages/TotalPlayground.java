package com.jst.packages;

import org.apache.commons.lang3.time.DateUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class TotalPlayground {

    public static void main(String[] args) throws InterruptedException, ParseException {
        Date d1 = new Date();
        TimeUnit.SECONDS.sleep(10);
        Date d2 = new Date();
        System.out.println();
        DateUtils.isSameDay(d1, d2);

        String input = "2012/01/20 12:05:10.321";
        DateFormat inputFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
        Date d3 = inputFormatter.parse(input);

        String input2 = "2020/04/27 12:05:10.321";
        Date d4 = inputFormatter.parse(input2);
    }
}
