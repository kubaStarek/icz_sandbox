package com.jst.packages;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PlayWithCollections {
    public static void main(String[] args) {
        Set<String> aSet = new HashSet<>();
        aSet.add("MyMapper");
        aSet.add("Bar");
        aSet.add("Car");

        List<String> aList = new ArrayList<>();
        aList.add("Bar");
        aList.add("Gogo");
        aList.add("Nogo");

        System.out.println(aList);
        System.out.println(aSet);

        aList.stream().forEach(e -> aSet.remove(e));

        System.out.println(aList);
        System.out.println(aSet);
    }
}
