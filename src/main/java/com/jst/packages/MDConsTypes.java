package com.jst.packages;

import java.util.Set;
import java.util.TreeSet;

public class MDConsTypes {

    private Set<OperationType> operations;

    public MDConsTypes() {
        operations = new TreeSet<>();
    }

    public void addOperation(OperationType operation){
        operations.add(operation);
    }

    public void merge(MDConsTypes mdConsTypes){
        this.operations.addAll(mdConsTypes.operations);
    }

    public boolean isEmpty(){
        return operations.size() == 0;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        operations.stream().forEach(o -> sb.append(o));
        return sb.toString();
    }

    public enum OperationType {
        M, N, O;
    }

    public static void main(String[] args) {
        MDConsTypes types = new MDConsTypes();
        types.addOperation(OperationType.M);
        System.out.println(types);

        MDConsTypes types2 = new MDConsTypes();
        types2.addOperation(OperationType.N);
        types2.addOperation(OperationType.O);
        types2.addOperation(OperationType.M);
        System.out.println(types2);

        types.merge(types2);
        System.out.println(types);
    }
}
