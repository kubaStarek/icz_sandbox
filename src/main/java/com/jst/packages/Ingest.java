package com.jst.packages;

import java.io.File;

public class Ingest {

    private static final String INGEST_LINE = "curl -v0 -u starekj@MP456:starekj -H \"Accept-Language: cs\" --data-binary @./%s -X POST \"http://localhost:8080/dea_frontend_war/rest/sipsubmission/submitpackage?userName=starekj&producerCode=MP456&producerSipId=%s\"";
    private static final String PATH = "C:\\Users\\starekj\\Desktop\\Balíčkový_sandbox";
    private static final String MARK = "balikBezSpisu";

    public static void main(String[] args) {
        prepareIngestScript(PATH);
    }

    private static void prepareIngestScript(String pathToPackageFolder){
        File packageFolder = new File(pathToPackageFolder);
        for (File f : packageFolder.listFiles()) {
            System.out.println(String.format(INGEST_LINE, f.getName(), MARK));
        }

    }

}
