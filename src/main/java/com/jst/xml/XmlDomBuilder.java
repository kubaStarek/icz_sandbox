package com.jst.xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;

public class XmlDomBuilder {

    private boolean omitXmlDeclaration;

    private Document document;

    private Element rootElement;

    public XmlDomBuilder(String rootElementName, boolean omitXmlDeclaration) throws ParserConfigurationException {
        this.omitXmlDeclaration = omitXmlDeclaration;
        document =  DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        rootElement = document.createElement(rootElementName);
        document.appendChild(rootElement);
    }

    public static XmlDomBuilder getInstance(String rootElementName) throws ParserConfigurationException {
        return new XmlDomBuilder(rootElementName, false);
    }

    public static XmlDomBuilder getInstance(String rootElementName, boolean omitXmlDeclaration) throws ParserConfigurationException {
        return new XmlDomBuilder(rootElementName, omitXmlDeclaration);
    }

    public XmlDomBuilder appendToRoot(String name, Object value){
        Element element = document.createElement(name);
        element.appendChild(document.createTextNode(value.toString()));
        rootElement.appendChild(element);
        return this;
    }

    public Element createElement(String name, Object value) {
        Element element = document.createElement(name);
        element.appendChild(document.createTextNode(value.toString()));
        return element;
    }

    public String toXml() throws TransformerException {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        if(omitXmlDeclaration) transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,  "yes");
        StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(document), new StreamResult(writer));
        return writer.getBuffer().toString();
    }
}
