package com.jst.rdctrl;



public class RdCntr {

    private Action action;

    private Integer period;

    public RdCntr(Action action, int period) {
        this.action = action;
        this.period = period;
    }

    public static RdCntr fromString(String input){
        if(!input.matches("[AVS]{1}[1-9]\\d*"))
            throw new RuntimeException("TODO");
        return new RdCntr(Action.valueOf(input.substring(0,1)), Integer.parseInt(input.substring(1)));
    }

    @Override
    public String toString() {
        return action.name()+period;
    }

    public void merge(String rdCntr){
        merge(fromString(rdCntr));
    }

    public void merge(RdCntr rdCntr){
        if(rdCntr == null) throw new RuntimeException("TODO");
        if(this.action.compareByWeight(rdCntr.action) < 0)
            this.action = rdCntr.action;
        if(this.period.compareTo(rdCntr.period) < 0)
            this.period = rdCntr.period;
    }

    public enum Action implements Comparable<Action>{
        S(10),
        V(50),
        A(100);

        private Integer weight;

        Action(Integer weight) {
            this.weight = weight;
        }

        public Integer getWeight() {
            return weight;
        }

        public int compareByWeight(Action o) {
            return  this.weight.compareTo(o.weight);
        }
    }
}
