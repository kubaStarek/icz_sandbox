package com.jst.mybatis.config;

import java.util.Properties;
import javax.sql.DataSource;
import org.apache.ibatis.datasource.DataSourceFactory;
import org.apache.ibatis.datasource.pooled.PooledDataSource;

public class DataSourceFactoryImpl implements DataSourceFactory {

    private Properties prop;

    @Override
    public DataSource getDataSource() {

        PooledDataSource ds = new PooledDataSource();

        ds.setDriver("oracle.jdbc.OracleDriver");
        ds.setUrl("jdbc:oracle:thin:@192.168.71.237:1521:DESA");
        ds.setUsername("DESA_STAREK");
        ds.setPassword("desa");

        return ds;
    }

    @Override
    public void setProperties(Properties prprts) {
        prop = prprts;
    }
}
