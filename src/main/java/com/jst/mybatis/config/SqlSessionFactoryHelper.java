package com.jst.mybatis.config;

import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;

import javax.sql.DataSource;

public class SqlSessionFactoryHelper {

    public static SqlSessionFactory getSqlSessionFactory(String mappersPackagePath) {

        DataSourceFactoryImpl mdsf = new DataSourceFactoryImpl();
        DataSource ds = mdsf.getDataSource();

        TransactionFactory trFact = new JdbcTransactionFactory();
        Environment environment = new Environment("development", trFact, ds);
        Configuration config = new Configuration(environment);
        config.addMappers(mappersPackagePath);

        return new SqlSessionFactoryBuilder().build(config);


    }

}
