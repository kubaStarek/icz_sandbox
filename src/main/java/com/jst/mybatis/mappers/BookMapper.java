package com.jst.mybatis.mappers;

import com.jst.mybatis.entity.Book;

import java.util.List;

public interface BookMapper {

    List<Book> getAll();

    void addBook(Book book);

    void addBooks(List<Book> books);
}
