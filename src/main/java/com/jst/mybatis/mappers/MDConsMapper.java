package com.jst.mybatis.mappers;

import com.jst.mybatis.entity.FixedMetadataVO;
import com.jst.mybatis.entity.MDConsGroupPackageVO;
import com.jst.mybatis.entity.MDConsPackage;
import com.jst.mybatis.entity.PackageDataForMDConsVO;
import com.jst.mybatis.entity.RecordWithSubfileDataVO;
import com.jst.mybatis.entity.SubfileForAutomaticMDConsVO;
import org.apache.ibatis.annotations.Param;

import java.sql.Timestamp;
import java.util.List;

public interface MDConsMapper {

    // TODO not finished yet
    List<RecordWithSubfileDataVO> findRecordsByParentAipvIds(List<String> parentAipvIds, @Param("producerId") Integer rowLimit);

    void insertGroupAndPackage(MDConsGroupPackageVO groupPackageVO);

    void insertPackage(MDConsPackage packageVO);

    List<FixedMetadataVO> getAllFixedMetadata();

    List<SubfileForAutomaticMDConsVO> findSubfilesForAutomaticMetadataConsolidation(@Param("producerId") int producerId);

    List<PackageDataForMDConsVO> findPackagesForMetadataConsolidation();

    void updateMetadataConsPackage(@Param("resultAipvId") String resultAipvId, @Param("packageId") int packageId);
}
