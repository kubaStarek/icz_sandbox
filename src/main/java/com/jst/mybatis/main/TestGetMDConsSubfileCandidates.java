package com.jst.mybatis.main;

import com.jst.mybatis.config.SqlSessionFactoryHelper;
import com.jst.mybatis.entity.SubfileForAutomaticMDConsVO;
import com.jst.mybatis.mappers.MDConsMapper;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class TestGetMDConsSubfileCandidates {
    public static void main(String[] args) {
        SqlSessionFactory sesFact = SqlSessionFactoryHelper.getSqlSessionFactory("com.jst.mybatis.mappers");
        try (SqlSession session = sesFact.openSession(ExecutorType.BATCH)) {
            MDConsMapper mapper = session.getMapper(MDConsMapper.class);

            List<SubfileForAutomaticMDConsVO> data = mapper.findSubfilesForAutomaticMetadataConsolidation(1001);
            System.out.println(data);
        }

    }

    private static void doPerfTest(){
        SqlSessionFactory sesFact = SqlSessionFactoryHelper.getSqlSessionFactory("com.jst.mybatis.mappers");
        try (SqlSession session = sesFact.openSession(ExecutorType.BATCH)) {
            MDConsMapper mapper = session.getMapper(MDConsMapper.class);
            Instant start = Instant.now();

            List<Integer> aList = new ArrayList<>();
            for (int i = 0; i < 200; i++) {
                aList.add(i);
            }

            aList.add(1001);

            for (Integer i: aList) {
                List<SubfileForAutomaticMDConsVO> data = mapper.findSubfilesForAutomaticMetadataConsolidation(i);
                System.out.println(data);
            }


            System.out.println("Select duration: "+ Duration.between(start, Instant.now()).toMillis()/1000 +" s");

        }
    }
}
