package com.jst.mybatis.main;

import com.jst.mybatis.config.SqlSessionFactoryHelper;
import com.jst.mybatis.entity.PackageDataForMDConsVO;
import com.jst.mybatis.mappers.MDConsMapper;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

public class TestFindPackagesForMetadataConsolidation {
    public static void main(String[] args) {
        SqlSessionFactory sesFact = SqlSessionFactoryHelper.getSqlSessionFactory("com.jst.mybatis.mappers");
        try (SqlSession session = sesFact.openSession(ExecutorType.BATCH)) {
            MDConsMapper mapper = session.getMapper(MDConsMapper.class);
            List<PackageDataForMDConsVO> data = mapper.findPackagesForMetadataConsolidation();
            for(PackageDataForMDConsVO vo : data){
                System.out.println(vo);
            }
            mapper.updateMetadataConsPackage("bbb", data.get(1).getPackageId());
            session.commit();
        }
    }
}
