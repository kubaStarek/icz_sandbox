package com.jst.mybatis.main;

import com.jst.mybatis.entity.MDConsPackage;
import com.jst.mybatis.executor.command.SqlCommand;
import com.jst.mybatis.executor.MBExecutorItem;
import com.jst.mybatis.config.SqlSessionFactoryHelper;
import com.jst.mybatis.entity.MDConsGroupPackageVO;
import com.jst.mybatis.executor.MyBatisBatchExecutor;
import com.jst.mybatis.mappers.MDConsMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

import static com.jst.mybatis.entity.DataHelper.prepareRecords;
import static com.jst.mybatis.entity.DataHelper.prepareSingleRecords;
import static com.jst.mybatis.entity.DataHelper.prepareSubfilePackages;

public class MDConsOperations {

    private List<MDConsGroupPackageVO> subfilePackages;

    private List<MDConsPackage> recordPackages;

    private List<MDConsGroupPackageVO> singleRecordPackages;

    private SqlSessionFactory sesFact;

    public MDConsOperations() {
        this.subfilePackages = prepareSubfilePackages(800);
        this.recordPackages = prepareRecords(subfilePackages, 20);
        this.singleRecordPackages = prepareSingleRecords(200);
        sesFact = SqlSessionFactoryHelper.getSqlSessionFactory("com.jst.mybatis.mappers");
    }

    public void store(){
            SqlCommand<MDConsGroupPackageVO> mdcGroupPackageCommand = (SqlSession session, MDConsGroupPackageVO vo) ->  session.getMapper(MDConsMapper.class).insertGroupAndPackage(vo);
            SqlCommand<MDConsPackage> mdcPackageCommand = (SqlSession session, MDConsPackage vo) ->  session.getMapper(MDConsMapper.class).insertPackage(vo);

            MyBatisBatchExecutor.getInstance(sesFact)
                .add(new MBExecutorItem(subfilePackages, mdcGroupPackageCommand))
                .add(new MBExecutorItem(recordPackages, mdcPackageCommand))
                .add(new MBExecutorItem(singleRecordPackages, mdcGroupPackageCommand))
                    .execute();
    }

    public static void main(String[] args) {
        MDConsOperations operations = new MDConsOperations();
        operations.store();
        // showTestData();
    }

    private static void showTestData(){
        MDConsOperations operations = new MDConsOperations();
        System.out.println("Subfiles\n------------------------------------------------------------");
        for (MDConsGroupPackageVO sf : operations.subfilePackages){
            System.out.println(sf);
        }

        System.out.println("\nRecords in subfiles\n------------------------------------------------------------");
        for(MDConsPackage rec: operations.recordPackages){
            System.out.println(rec);
        }

        System.out.println("\nSingles records\n------------------------------------------------------------");
        for (MDConsGroupPackageVO sr : operations.singleRecordPackages){
            System.out.println(sr);
        }
    }

}
