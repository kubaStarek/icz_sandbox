package com.jst.mybatis.main;

import com.jst.mybatis.config.SqlSessionFactoryHelper;
import com.jst.mybatis.entity.FixedMetadataVO;
import com.jst.mybatis.mappers.MDConsMapper;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

public class TestGetAllFixedData {

    public static void main(String[] args) {
        SqlSessionFactory sesFact = SqlSessionFactoryHelper.getSqlSessionFactory("com.jst.mybatis.mappers");
        try (SqlSession session = sesFact.openSession(ExecutorType.BATCH)) {
            MDConsMapper mapper = session.getMapper(MDConsMapper.class);
            List<FixedMetadataVO> data = mapper.getAllFixedMetadata();
            for(FixedMetadataVO vo : data){
                System.out.println(vo);
            }

        }
    }
}
