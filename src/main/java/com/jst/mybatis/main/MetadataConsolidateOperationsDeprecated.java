package com.jst.mybatis.main;

import com.jst.mybatis.config.SqlSessionFactoryHelper;
import com.jst.mybatis.entity.MDConsGroupPackageVO;
import com.jst.mybatis.entity.MDConsPackage;
import com.jst.mybatis.mappers.MDConsMapper;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;

public class MetadataConsolidateOperationsDeprecated {

    private static final String CLOB = "<MDConsValues><DateDisposal>16.09.29 00:00:00</DateDisposal><DateClosed>20.10.29 00:00:00</DateClosed><RdCntrlAcr>A10</RdCntrlAcr></MDConsValues>";

    private SqlSessionFactory sesFact;

    private Timestamp timePreparedGroup;

    public MetadataConsolidateOperationsDeprecated(){
        sesFact = SqlSessionFactoryHelper.getSqlSessionFactory("com.jst.mybatis.mappers");
    }

    public void store(boolean batch){
        try (SqlSession session = batch ? sesFact.openSession(ExecutorType.BATCH) : sesFact.openSession()) {
            timePreparedGroup = new Timestamp(System.currentTimeMillis());
            MDConsMapper mapper = session.getMapper(MDConsMapper.class);

            storeSubfiles(mapper, session);
            storeRecords(mapper, session);
            storeRecordsWithoutSubfile(mapper, session);

            session.commit();
        }
    }

    public void storeSubfiles(MDConsMapper mapper, SqlSession session){
        Instant start = Instant.now();
        int counter;
        for (int i = 0; i < 2 ; i++) {
            for (int j = 0; j < 1000 ; j++) {
                counter = 1000*i+j;
                mapper.insertGroupAndPackage(
                        new MDConsGroupPackageVO("ErmsUid"+counter, "MNO", CLOB, "aipvIdS"+counter, "aipvIdS_result"+counter, new Timestamp(System.currentTimeMillis()), "O", timePreparedGroup));
                        //new MDConsGroupPackageVO("ErmsUid"+counter, "MNO", CLOB, "aipvIdS"+counter, "O"));
            }
            session.flushStatements();
            session.clearCache();
        }

        System.out.println("Store subfiles duration: "+Duration.between(start, Instant.now()).toMillis()+" ms");
    }

    public void storeRecords(MDConsMapper mapper, SqlSession session){
        Instant start = Instant.now();
        int counter, period, forkey;
        for (int i = 0; i < 2 ; i++) {
            period = i%3;
            for (int j = 0; j < 1000 ; j++) {
                counter = 1000*i+j;
                forkey = 1000*period+j;
                mapper.insertPackage(
                    new MDConsPackage("aipvIdR"+counter, "aipvIdS"+forkey, "aipvIdR_result"+counter, new Timestamp(System.currentTimeMillis()),"MN", timePreparedGroup));
                    //new MDConsPackage("aipvIdR"+counter, "aipvIdS"+forkey, "MN"));
            }
            session.flushStatements();
            session.clearCache();
        }


        System.out.println("Store records duration: "+Duration.between(start, Instant.now()).toMillis()+" ms");
    }

    public void storeRecordsWithoutSubfile(MDConsMapper mapper, SqlSession session){
        Instant start = Instant.now();
        int counter;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 1000 ; j++) {
                counter = 1000*i+j;
                mapper.insertGroupAndPackage(
                        new MDConsGroupPackageVO("ErmsUidPWS"+counter, "MNO", CLOB, "aipvIdRWS"+counter, "MNO", timePreparedGroup));
            }
            session.flushStatements();
            session.clearCache();
        }
        System.out.println("Store records without subfile duration: "+Duration.between(start, Instant.now()).toMillis()+" ms");
    }

    public static void main(String[] args) {
        MetadataConsolidateOperationsDeprecated operations = new MetadataConsolidateOperationsDeprecated();
        operations.store(true);
        //operations.store(false);
    }
}
