package com.jst.mybatis.main;

import com.jst.mybatis.config.SqlSessionFactoryHelper;
import com.jst.mybatis.entity.Book;
import com.jst.mybatis.mappers.BookMapper;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.time.Duration;
import java.time.Instant;

public class BookOperations {

    private static SqlSessionFactory sesFact;

    static {
        sesFact = SqlSessionFactoryHelper.getSqlSessionFactory("com.jst.mybatis.mappers");
    }

    public static void main(String[] args) {
        System.out.printf("Batch insert duration %d ms\n", bulkInsert());
        //System.out.printf("Non-batch insert duration %d ms", bulkInsert_noBatch());
    }

    private static long bulkInsert(){
        Instant start = Instant.now();
        try (SqlSession session = sesFact.openSession(ExecutorType.BATCH)) {
            BookMapper bookMapper = session.getMapper(BookMapper.class);
            for (int i = 0; i < 1000; i++) {
                bookMapper.addBook(new Book("book"+i, "author"+i));
            }
            session.commit();
        }
        Instant finish = Instant.now();
        return Duration.between(start, finish).toMillis();
}

    private static long bulkInsert_noBatch(){
        Instant start = Instant.now();
        try (SqlSession session = sesFact.openSession()) {
            BookMapper bookMapper = session.getMapper(BookMapper.class);
            for (int i = 0; i < 1000; i++) {
                bookMapper.addBook(new Book("book"+i, "author"+i));
            }
            session.commit();
        }
        Instant finish = Instant.now();
        return Duration.between(start, finish).toMillis();
    }

}
