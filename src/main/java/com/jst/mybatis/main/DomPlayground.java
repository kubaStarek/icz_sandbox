package com.jst.mybatis.main;

import com.jst.xml.XmlDomBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.sql.Timestamp;

public class DomPlayground {

    private static final String ROOT_ELEMENT_NAME = "MDConsValues";
    private static final String DATE_DISPOSAL_ELEMENT_NAME = "DateDisposal";
    private static final String DATE_CLOSED_ELEMENT_NAME = "DateClosed";
    private static final String RD_CNTRL_ACR_ELEMENT_NAME = "RdCntrlAcr";

    public static void main(String[] args) throws ParserConfigurationException, TransformerException {
        String foo = XmlDomBuilder.getInstance(ROOT_ELEMENT_NAME, true)
                .appendToRoot(DATE_DISPOSAL_ELEMENT_NAME, new Timestamp(System.currentTimeMillis()))
                .appendToRoot(DATE_CLOSED_ELEMENT_NAME, new Timestamp(System.currentTimeMillis()))
                .appendToRoot(RD_CNTRL_ACR_ELEMENT_NAME, "A10")
                .toXml();
        System.out.println(foo);

    }

    public String toXml() throws ParserConfigurationException, TransformerException {
        DocumentBuilder docBuilder =  DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement(ROOT_ELEMENT_NAME);
        doc.appendChild(rootElement);

        Element dateDisposal = doc.createElement(DATE_DISPOSAL_ELEMENT_NAME);
        dateDisposal.appendChild(doc.createTextNode("1"));
        rootElement.appendChild(dateDisposal);

        Element dateClosed = doc.createElement(DATE_CLOSED_ELEMENT_NAME);
        dateClosed.appendChild(doc.createTextNode("2"));
        rootElement.appendChild(dateClosed);

        Element rdCntrlAcr = doc.createElement(RD_CNTRL_ACR_ELEMENT_NAME);
        rdCntrlAcr.appendChild(doc.createTextNode("3"));
        rootElement.appendChild(rdCntrlAcr);

        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(doc), new StreamResult(writer));
        return writer.getBuffer().toString();

    }
}
