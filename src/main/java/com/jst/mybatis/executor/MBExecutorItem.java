package com.jst.mybatis.executor;

import com.jst.mybatis.executor.command.SqlCommand;

import java.util.List;

public class MBExecutorItem<VO> {
    private List<VO> data;

    private SqlCommand<VO> command;

    public MBExecutorItem(List<VO> data, SqlCommand<VO> command) {
        this.data = data;
        this.command = command;
    }

    public List<VO> getData() {
        return data;
    }

    public void setData(List<VO> data) {
        this.data = data;
    }

    public SqlCommand<VO> getCommand() {
        return command;
    }

    public void setCommand(SqlCommand<VO> command) {
        this.command = command;
    }
}