package com.jst.mybatis.executor.command;

import org.apache.ibatis.session.SqlSession;

@FunctionalInterface
public interface SqlCommand<VO> {
    void executeSql(SqlSession session, VO vo);
}
