package com.jst.mybatis.executor;

import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.time.Duration;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;

public class MyBatisBatchExecutor {

    private static final int MAX_PARTITION_SIZE = 1000;

    private SqlSessionFactory sesFact;

    private int partitionSize;

    private List<MBExecutorItem> mbExecutorItems;

    public static MyBatisBatchExecutor getInstance(SqlSessionFactory sesFact, int partitionSize){
        return new MyBatisBatchExecutor(sesFact, partitionSize);
    }

    public static MyBatisBatchExecutor getInstance(SqlSessionFactory sesFact){
        return new MyBatisBatchExecutor(sesFact, MAX_PARTITION_SIZE);
    }

    private MyBatisBatchExecutor(SqlSessionFactory sesFact, int partitionSize) {
        setPartitionSize(partitionSize);
        this.sesFact = sesFact;
        mbExecutorItems = new LinkedList<>();
    }

    public MyBatisBatchExecutor add(MBExecutorItem item){
        mbExecutorItems.add(item);
        return this;
    }

    public void execute(){
        try (SqlSession session = sesFact.openSession(ExecutorType.BATCH)) {
            for (MBExecutorItem item: mbExecutorItems) {
                executeItemWithPartitioning(session, item);
            }
            session.commit();
        }
    }

    private <VO> void executeItemWithPartitioning(SqlSession session, MBExecutorItem<VO> executorItem){
        Instant start = Instant.now();
        int counter = 0;
        for (VO vo: executorItem.getData()) {
            counter++;
            executorItem.getCommand().executeSql(session, vo);
            if(counter%partitionSize == 0){
                session.flushStatements();
                session.clearCache();
            }
        }
        System.out.println("Store duration: "+ Duration.between(start, Instant.now()).toMillis()+" ms");
    }

    public int getPartitionSize() {
        return partitionSize;
    }

    public void setPartitionSize(int partitionSize) {
        if(partitionSize < 1 || partitionSize > MAX_PARTITION_SIZE)
            throw new IllegalArgumentException("Param partitionSize must be in range 1 to 1000.");
        this.partitionSize = partitionSize;
    }
}
