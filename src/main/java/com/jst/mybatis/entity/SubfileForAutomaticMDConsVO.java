package com.jst.mybatis.entity;

import java.sql.Timestamp;

public class SubfileForAutomaticMDConsVO {

    private int aoId;

    private String aipvId;

    private String ermsUid;

    private String  recclFullyqcc;

    private String  rdcntrlAcr;

    private Timestamp rdEffectiveDate;

    private Timestamp dateDisposal;

    public SubfileForAutomaticMDConsVO() {
    }

    public SubfileForAutomaticMDConsVO(int aoId, String aipvId, String ermsUid, String recclFullyqcc, String rdcntrlAcr, Timestamp rdEffectiveDate, Timestamp dateDisposal) {
        this.aoId = aoId;
        this.aipvId = aipvId;
        this.ermsUid = ermsUid;
        this.recclFullyqcc = recclFullyqcc;
        this.rdcntrlAcr = rdcntrlAcr;
        this.rdEffectiveDate = rdEffectiveDate;
        this.dateDisposal = dateDisposal;
    }

    public int getAoId() {
        return aoId;
    }

    public void setAoId(int aoId) {
        this.aoId = aoId;
    }

    public String getAipvId() {
        return aipvId;
    }

    public void setAipvId(String aipvId) {
        this.aipvId = aipvId;
    }

    public String getErmsUid() {
        return ermsUid;
    }

    public void setErmsUid(String ermsUid) {
        this.ermsUid = ermsUid;
    }

    public String getRecclFullyqcc() {
        return recclFullyqcc;
    }

    public void setRecclFullyqcc(String recclFullyqcc) {
        this.recclFullyqcc = recclFullyqcc;
    }

    public String getRdcntrlAcr() {
        return rdcntrlAcr;
    }

    public void setRdcntrlAcr(String rdcntrlAcr) {
        this.rdcntrlAcr = rdcntrlAcr;
    }

    public Timestamp getRdEffectiveDate() {
        return rdEffectiveDate;
    }

    public void setRdEffectiveDate(Timestamp rdEffectiveDate) {
        this.rdEffectiveDate = rdEffectiveDate;
    }

    public Timestamp getDateDisposal() {
        return dateDisposal;
    }

    public void setDateDisposal(Timestamp dateDisposal) {
        this.dateDisposal = dateDisposal;
    }

    @Override
    public String toString() {
        return "SubfileForAutomaticMDConsVO{" +
                "aoId=" + aoId +
                ", aipvId='" + aipvId + '\'' +
                ", ermsUid='" + ermsUid + '\'' +
                ", recclFullyqcc='" + recclFullyqcc + '\'' +
                ", rdcntrlAcr='" + rdcntrlAcr + '\'' +
                ", rdEffectiveDate=" + rdEffectiveDate +
                ", dateDisposal=" + dateDisposal +
                '}';
    }
}
