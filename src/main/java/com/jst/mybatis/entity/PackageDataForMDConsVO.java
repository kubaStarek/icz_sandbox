package com.jst.mybatis.entity;

public class PackageDataForMDConsVO {

    private int packageId;

    private String aipvIdPackage;

    private String consTypes;

    private String consValues;

    private int groupId;

    private String ermsUidPackage;

    private int producerId;

    private String userId;

    public PackageDataForMDConsVO() {
    }

    public PackageDataForMDConsVO(int packageId, String aipvIdPackage, String consTypes, String consValues, int groupId, String ermsUidPackage, int producerId, String userId) {
        this.packageId = packageId;
        this.aipvIdPackage = aipvIdPackage;
        this.consTypes = consTypes;
        this.consValues = consValues;
        this.groupId = groupId;
        this.ermsUidPackage = ermsUidPackage;
        this.producerId = producerId;
        this.userId = userId;
    }

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    public String getAipvIdPackage() {
        return aipvIdPackage;
    }

    public void setAipvIdPackage(String aipvIdPackage) {
        this.aipvIdPackage = aipvIdPackage;
    }

    public String getConsTypes() {
        return consTypes;
    }

    public void setConsTypes(String consTypes) {
        this.consTypes = consTypes;
    }

    public String getConsValues() {
        return consValues;
    }

    public void setConsValues(String consValues) {
        this.consValues = consValues;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getErmsUidPackage() {
        return ermsUidPackage;
    }

    public void setErmsUidPackage(String ermsUidPackage) {
        this.ermsUidPackage = ermsUidPackage;
    }

    public int getProducerId() {
        return producerId;
    }

    public void setProducerId(int producerId) {
        this.producerId = producerId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "PackageDataForMDConsVO{" +
                "packageId=" + packageId +
                ", aipvIdPackage='" + aipvIdPackage + '\'' +
                ", consTypes='" + consTypes + '\'' +
                ", consValues='" + consValues + '\'' +
                ", groupId=" + groupId +
                ", ermsUidPackage='" + ermsUidPackage + '\'' +
                ", producerId=" + producerId +
                ", userId='" + userId + '\'' +
                '}';
    }
}
