package com.jst.mybatis.entity;

import java.sql.Timestamp;

public class MDConsPackage {

    private String aipvIdPackage;

    private String aipvIdGroup;

    private String resultAipvIdPackage;

    private Timestamp timeExecuted;

    private String consTypes;

    private Timestamp timePreparedGroup;

    public MDConsPackage() {}


    public MDConsPackage(String aipvIdPackage, String aipvIdGroup, String consTypes, Timestamp timePreparedGroup) {
        this(aipvIdPackage, aipvIdGroup, null, null, consTypes, timePreparedGroup);
    }

    public MDConsPackage(String aipvIdPackage, String aipvIdGroup, String resultAipvIdPackage, Timestamp timeExecuted, String consTypes, Timestamp timePreparedGroup) {
        this.aipvIdPackage = aipvIdPackage;
        this.aipvIdGroup = aipvIdGroup;
        this.timePreparedGroup = timePreparedGroup;
        this.resultAipvIdPackage = resultAipvIdPackage;
        this.timeExecuted = timeExecuted;
        this.consTypes = consTypes;
    }

    public String getAipvIdPackage() {
        return aipvIdPackage;
    }

    public void setAipvIdPackage(String aipvIdPackage) {
        this.aipvIdPackage = aipvIdPackage;
    }

    public String getAipvIdGroup() {
        return aipvIdGroup;
    }

    public void setAipvIdGroup(String aipvIdGroup) {
        this.aipvIdGroup = aipvIdGroup;
    }

    public Timestamp getTimePreparedGroup() {
        return timePreparedGroup;
    }

    public void setTimePreparedGroup(Timestamp timePreparedGroup) {
        this.timePreparedGroup = timePreparedGroup;
    }

    public String getResultAipvIdPackage() {
        return resultAipvIdPackage;
    }

    public void setResultAipvIdPackage(String resultAipvIdPackage) {
        this.resultAipvIdPackage = resultAipvIdPackage;
    }

    public Timestamp getTimeExecuted() {
        return timeExecuted;
    }

    public void setTimeExecuted(Timestamp timeExecuted) {
        this.timeExecuted = timeExecuted;
    }

    public String getConsTypes() {
        return consTypes;
    }

    public void setConsTypes(String consTypes) {
        this.consTypes = consTypes;
    }

    @Override
    public String toString() {
        return "MDConsPackage{" +
                "aipvIdPackage='" + aipvIdPackage + '\'' +
                ", aipvIdGroup='" + aipvIdGroup + '\'' +
                ", timePreparedGroup=" + timePreparedGroup +
                ", resultAipvIdPackage='" + resultAipvIdPackage + '\'' +
                ", timeExecuted=" + timeExecuted +
                ", consTypes='" + consTypes + '\'' +
                '}';
    }
}
