package com.jst.mybatis.entity;

import java.sql.Timestamp;

public class MDConsGroupPackageVO {

    private String containerErmsUid;

    private String consTypesGroup;

    private String consValues;

    private String aipvId;

    private String resultAipvId;

    private Timestamp timeExecuted;

    private String consTypesPackage;

    private Timestamp timePreparedGroup;

    public MDConsGroupPackageVO(){}

    public MDConsGroupPackageVO(String containerErmsUid, String consTypesGroup, String consValues, String aipvId, String consTypesPackage, Timestamp timePreparedGroup) {
        this( containerErmsUid, consTypesGroup, consValues, aipvId, null, null, consTypesPackage, timePreparedGroup);
    }

    public MDConsGroupPackageVO(String containerErmsUid, String consTypesGroup, String consValues, String aipvId, String resultAipvId, Timestamp timeExecuted, String consTypesPackage, Timestamp timePreparedGroup) {
        this.containerErmsUid = containerErmsUid;
        this.consTypesGroup = consTypesGroup;
        this.consValues = consValues;
        this.aipvId = aipvId;
        this.resultAipvId = resultAipvId;
        this.timeExecuted = timeExecuted;
        this.consTypesPackage = consTypesPackage;
        this.timePreparedGroup = timePreparedGroup;
    }

    public String getContainerErmsUid() {
        return containerErmsUid;
    }

    public void setContainerErmsUid(String containerErmsUid) {
        this.containerErmsUid = containerErmsUid;
    }

    public String getConsTypesGroup() {
        return consTypesGroup;
    }

    public void setConsTypesGroup(String consTypesGroup) {
        this.consTypesGroup = consTypesGroup;
    }

    public String getConsValues() {
        return consValues;
    }

    public void setConsValues(String consValues) {
        this.consValues = consValues;
    }

    public String getAipvId() {
        return aipvId;
    }

    public void setAipvId(String aipvId) {
        this.aipvId = aipvId;
    }

    public String getResultAipvId() {
        return resultAipvId;
    }

    public void setResultAipvId(String resultAipvId) {
        this.resultAipvId = resultAipvId;
    }

    public Timestamp getTimeExecuted() {
        return timeExecuted;
    }

    public void setTimeExecuted(Timestamp timeExecuted) {
        this.timeExecuted = timeExecuted;
    }

    public String getConsTypesPackage() {
        return consTypesPackage;
    }

    public void setConsTypesPackage(String consTypesPackage) {
        this.consTypesPackage = consTypesPackage;
    }
    public Timestamp getTimePreparedGroup() {
        return timePreparedGroup;
    }

    public void setTimePreparedGroup(Timestamp timePreparedGroup) {
        this.timePreparedGroup = timePreparedGroup;
    }

    @Override
    public String toString() {
        return "MDConsGroupPackageVO{" +
                "containerErmsUid='" + containerErmsUid + '\'' +
                ", consTypesGroup='" + consTypesGroup + '\'' +
                ", consValues='" + consValues + '\'' +
                ", aipvId='" + aipvId + '\'' +
                ", resultAipvId='" + resultAipvId + '\'' +
                ", timeExecuted=" + timeExecuted +
                ", consTypesPackage='" + consTypesPackage + '\'' +
                ", timePreparedGroup=" + timePreparedGroup +
                '}';
    }
}
