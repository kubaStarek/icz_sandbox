package com.jst.mybatis.entity;

import java.sql.Timestamp;

public class FixedMetadataVO {

    private Timestamp timePrepared;

    private String  producerFileId;

    private int consPackageCount;

    private String  consTypes;

    private Timestamp timeExecuted;

    public FixedMetadataVO(){}

    public FixedMetadataVO(Timestamp timePrepared, String producerFileId, int consPackageCount, String consTypes, Timestamp timeExecuted) {
        this.timePrepared = timePrepared;
        this.producerFileId = producerFileId;
        this.consPackageCount = consPackageCount;
        this.consTypes = consTypes;
        this.timeExecuted = timeExecuted;
    }

    public Timestamp getTimePrepared() {
        return timePrepared;
    }

    public void setTimePrepared(Timestamp timePrepared) {
        this.timePrepared = timePrepared;
    }

    public String getProducerFileId() {
        return producerFileId;
    }

    public void setProducerFileId(String producerFileId) {
        this.producerFileId = producerFileId;
    }

    public int getConsPackageCount() {
        return consPackageCount;
    }

    public void setConsPackageCount(int consPackageCount) {
        this.consPackageCount = consPackageCount;
    }

    public String getConsTypes() {
        return consTypes;
    }

    public void setConsTypes(String consTypes) {
        this.consTypes = consTypes;
    }

    public Timestamp getTimeExecuted() {
        return timeExecuted;
    }

    public void setTimeExecuted(Timestamp timeExecuted) {
        this.timeExecuted = timeExecuted;
    }

    @Override
    public String toString() {
        return "FixedMetadataVO{" +
                "timePrepared=" + timePrepared +
                ", producerFileId='" + producerFileId + '\'' +
                ", consPackageCount=" + consPackageCount +
                ", consTypes='" + consTypes + '\'' +
                ", timeExecuted=" + timeExecuted +
                '}';
    }
}
