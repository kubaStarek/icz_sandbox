package com.jst.mybatis.entity;

import java.util.Date;

public class RecordWithSubfileDataVO {

    private String aipvIdRecord;

    private String aipvIdSubfile;

    private String rdcntrlAcrRecord;

    private String rdcntrlAcrSubfile;

    private Date dateDisposalRecord;

    private Date dateDisposalSubfile;

    private Date  dateClosedSubfile;

    private String ermsUidSubfile;

    public RecordWithSubfileDataVO() {
    }

    public RecordWithSubfileDataVO(String aipvIdRecord, String aipvIdSubfile, String rdcntrlAcrRecord, String rdcntrlAcrSubfile, Date dateDisposalRecord, Date dateDisposalSubfile, Date dateClosedSubfile, String ermsUidSubfile) {
        this.aipvIdRecord = aipvIdRecord;
        this.aipvIdSubfile = aipvIdSubfile;
        this.rdcntrlAcrRecord = rdcntrlAcrRecord;
        this.rdcntrlAcrSubfile = rdcntrlAcrSubfile;
        this.dateDisposalRecord = dateDisposalRecord;
        this.dateDisposalSubfile = dateDisposalSubfile;
        this.dateClosedSubfile = dateClosedSubfile;
        this.ermsUidSubfile = ermsUidSubfile;
    }

    public String getAipvIdRecord() {
        return aipvIdRecord;
    }

    public void setAipvIdRecord(String aipvIdRecord) {
        this.aipvIdRecord = aipvIdRecord;
    }

    public String getAipvIdSubfile() {
        return aipvIdSubfile;
    }

    public void setAipvIdSubfile(String aipvIdSubfile) {
        this.aipvIdSubfile = aipvIdSubfile;
    }

    public String getRdcntrlAcrRecord() {
        return rdcntrlAcrRecord;
    }

    public void setRdcntrlAcrRecord(String rdcntrlAcrRecord) {
        this.rdcntrlAcrRecord = rdcntrlAcrRecord;
    }

    public String getRdcntrlAcrSubfile() {
        return rdcntrlAcrSubfile;
    }

    public void setRdcntrlAcrSubfile(String rdcntrlAcrSubfile) {
        this.rdcntrlAcrSubfile = rdcntrlAcrSubfile;
    }

    public Date getDateDisposalRecord() {
        return dateDisposalRecord;
    }

    public void setDateDisposalRecord(Date dateDisposalRecord) {
        this.dateDisposalRecord = dateDisposalRecord;
    }

    public Date getDateDisposalSubfile() {
        return dateDisposalSubfile;
    }

    public void setDateDisposalSubfile(Date dateDisposalSubfile) {
        this.dateDisposalSubfile = dateDisposalSubfile;
    }

    public Date getDateClosedSubfile() {
        return dateClosedSubfile;
    }

    public void setDateClosedSubfile(Date dateClosedSubfile) {
        this.dateClosedSubfile = dateClosedSubfile;
    }

    public String getErmsUidSubfile() {
        return ermsUidSubfile;
    }

    public void setErmsUidSubfile(String ermsUidSubfile) {
        this.ermsUidSubfile = ermsUidSubfile;
    }

    @Override
    public String toString() {
        return "RecordWithSubfileDataVO{" +
                "aipvIdRecord='" + aipvIdRecord + '\'' +
                ", aipvIdSubfile='" + aipvIdSubfile + '\'' +
                ", rdcntrlAcrRecord='" + rdcntrlAcrRecord + '\'' +
                ", rdcntrlAcrSubfile='" + rdcntrlAcrSubfile + '\'' +
                ", dateDisposalRecord=" + dateDisposalRecord +
                ", dateDisposalSubfile=" + dateDisposalSubfile +
                ", dateClosedSubfile=" + dateClosedSubfile +
                ", ermsUidSubfile='" + ermsUidSubfile + '\'' +
                '}';
    }
}
