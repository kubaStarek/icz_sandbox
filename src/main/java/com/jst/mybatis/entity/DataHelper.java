package com.jst.mybatis.entity;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class DataHelper {

    private static final String  CONS_VALUES = "<MDConsValues><DateDisposal>16.09.29 00:00:00</DateDisposal><DateClosed>20.10.29 00:00:00</DateClosed><RdCntrlAcr>A10</RdCntrlAcr></MDConsValues>";
    private static Timestamp timePreparedGroup = new Timestamp(System.currentTimeMillis());

    public static List<MDConsGroupPackageVO> prepareSubfilePackages(int size){
        List<MDConsGroupPackageVO> subfilePackages = new ArrayList<>();
        for (int i = 1; i <= size ; i++) {
            subfilePackages.add(new MDConsGroupPackageVO("ErmsUid"+i, "MNO", CONS_VALUES, "aipvIdS"+i, "O", timePreparedGroup));
        }
        return subfilePackages;
    }

    public static List<MDConsPackage> prepareRecords(List<MDConsGroupPackageVO> subfiles, int recordsInOneSubfile){
        List<MDConsPackage> records = new ArrayList<>();
        int counter = 0;
        for (MDConsGroupPackageVO subfile : subfiles){
            for (int i = 1; i <= recordsInOneSubfile ; i++) {
                counter++;
                records.add(new MDConsPackage("aipvIdR"+counter, subfile.getAipvId(), "MN", timePreparedGroup));
            }
        }
        return records;
    }

    public static List<MDConsGroupPackageVO> prepareSingleRecords(int size){
        List<MDConsGroupPackageVO> singleDocuments = new ArrayList<>();

        for (int i = 1; i <= size ; i++) {
            singleDocuments.add(new MDConsGroupPackageVO("ErmsUidPWS"+i, "MNO", CONS_VALUES, "aipvIdRWS"+i, "MNO", timePreparedGroup));
        }
        return singleDocuments;
    }

}
