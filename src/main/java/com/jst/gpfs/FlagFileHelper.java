package com.jst.gpfs;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FlagFileHelper {

    public static void main(String[] args) {
       FlagFileType foo = new FlagFileHelper().getCurrentFlag("C:\\work\\DESA_FOLDERS\\archive2");
        System.out.println(foo);

        List<FlagFileType> discoveredFlagFiles = Arrays.asList(FlagFileType.CLOSED_RECENTLY, FlagFileType.ACTIVE);
        System.out.println(String.format("Multiple flag files %s found in storage %s", discoveredFlagFiles, "VPL"));

    }

    public FlagFileType getCurrentFlag(String path) {
        Path rootPath = Paths.get(path);

        List<FlagFileType> discoveredFlagFiles = Arrays.stream(FlagFileType.values())
                .filter( flagFile -> flagFile.getFilename() != null && Files.exists(rootPath.resolve(flagFile.getFilename())))
                .collect(Collectors.toList());

        switch (discoveredFlagFiles.size()) {
            case 0:
                return FlagFileType.CLOSED;
            case 1:
                return discoveredFlagFiles.get(0);
            default:
                throw new RuntimeException("too many flag files "+discoveredFlagFiles);

        }
    }
}
