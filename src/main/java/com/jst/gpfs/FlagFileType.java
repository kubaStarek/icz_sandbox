package com.jst.gpfs;

public enum FlagFileType {
    READY("_ready", "ready"),
    ACTIVE("_active", "active"),
    CLOSED_RECENTLY("_closedRecently", "recently_closed"),
    CLOSED(null,"closed");

    private String filename;
    private String description;

    FlagFileType(String filename, String description) {
        this.filename = filename;
        this.description = description;
    }

    public String getFilename() {
        return filename;
    }

    public String getDescription() {
        return description;
    }
}
