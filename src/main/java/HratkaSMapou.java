import java.util.LinkedHashMap;
import java.util.Map;

public class HratkaSMapou {

    public static void main(String[] args) {
        Map<String, Object> aMap = new LinkedHashMap<>();
        aMap.put("a", 1);
        aMap.put("b", 2);
        aMap.put("c", 3);

        Map<String, Object> aMap2 = new LinkedHashMap<>();
        aMap.put("a", 4);
        aMap.put("c", 6);

        aMap.putAll(aMap2);

        System.out.println(aMap);

    }
}
