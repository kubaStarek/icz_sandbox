public class ProductVariantHelper {

    public static void main(String[] args) {
        System.out.println(isDesaOrSprs(""));
        System.out.println(isDesaOrSprs("sprs"));
        System.out.println(isDesaOrSprs(null));
        System.out.println(isDesaOrSprsV2(""));
        System.out.println(isDesaOrSprsV2("sprs"));
        System.out.println(isDesaOrSprsV2(null));

    }

    private static boolean isDesaOrSprs(String variant){
        // "" stands for DESA .. unknown reason it's not set in beans-configuration.xml unlike other variants
        if(variant == null) return false;
        switch (variant){
            case "":
            case "sprs":
                return true;
            default:
                return false;
        }
    }

    private static boolean isDesaOrSprsV2(String variant){
        // "" stands for DESA .. unknown reason it's not set in beans-configuration.xml unlike other variants
        return ("".equals(variant) || "sprs".equals(variant));
    }
}
