package bar.zar;

public enum ABWrapper {
    A(bar.zar.A.class),
    B(bar.zar.B.class);

    private Class<?> clazz;

    ABWrapper(Class<?> clazz) {
        this.clazz = clazz;
    }

    public static ABWrapper valueOf(Object o){
        if(o == null) return null;

        for(ABWrapper item: values()){
            if(item.clazz.equals(o.getClass())) return item;
        }

        return null;
    }
}
